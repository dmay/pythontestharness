
#include "stdio.h"

int main(void)
{

  printf("@ -- \n");
  printf("@ -- Output from fake application code \n");
  printf("@ -- comment lines \n");
  printf("@ -- \n");
 
  printf("$cputime = { 2.2 }\n");
  printf("$residuals { 33.33, 6.6 }\n");
  printf("$kspits 42\n");
  printf("$norm = 99.9\n");
  printf("$rms 51.0\n");
  
  return(0);
}
